
const fs = require('fs');

class jsonController {
    static listJsonItems = async (req, res) =>{
        try {
            const { default: journeyData } = await import("../json/journeyData.json", {
                assert: {
                  type: "json",
                },
              });
            res.status(200).json(journeyData)
        } catch (err) {

            res.status(500).json({message: 'Erro ao ler arquivo json.', err})

        }
    }
    static addJsonItem = async (req, res) =>{
        let { name } = req.body
        const { default: journeyData } = await import("../json/journeyData.json", {
            assert: {
              type: "json",
            },
          });
        let newItem = { 
            name,
            id: journeyData[journeyData.length - 1].id + 1 
        };
        try {
            journeyData.push(newItem)
            fs.writeFileSync(__dirname + '/../json/journeyData.json', JSON.stringify(journeyData))
        } catch (err) {
            res.status(500).json({message: 'Erro ao redefinir arquivo json.', err})
        }finally{
            res.status(200).json(journeyData)
        }
    }
    static removeJsonItem = async (req, res) =>{
        const { default: journeyData } = await import("../json/journeyData.json", {
            assert: {
              type: "json",
            },
        });
        let itemId = req.params.id
        let deleteItemIndex = journeyData.findIndex(item => item.id = itemId)
        journeyData.splice(deleteItemIndex, 1)
        try {
            fs.writeFileSync(__dirname + '/../json/journeyData.json', JSON.stringify(journeyData))
        } catch (error) {
            res.status(500).json({message: 'Erro ao redefinir arquivo json.', err})
        }finally{
            res.status(200).json(journeyData)
        }

    }
}

module.exports = jsonController