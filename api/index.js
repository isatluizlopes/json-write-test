const express = require("express");
const cors = require("cors");
const routes = require("./Routes/index.js");
const PORT = 8080 || 3001;
const app = express();
app.use(express.json());
app.use(
  cors({
    origin: "*",
  })
);
routes(app);
app.listen(PORT, () => {
  console.log(`Servidor rodando, porta: ${PORT}`);
});
