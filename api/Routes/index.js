const jsonRoutes = require('./jsonRoutes.js')
const routes = (app) =>{
    app.use(
        jsonRoutes
    )
}
module.exports = routes