const express = require('express')
const router = express.Router()
const jsonController = require('../Controllers/jsonController')
router
.get('/json/list', jsonController.listJsonItems)
.post('/json/add', jsonController.addJsonItem)
.delete('/json/delete/:id', jsonController.removeJsonItem)

module.exports = router